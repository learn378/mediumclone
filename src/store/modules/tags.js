import tagsApi from '@/api/tags'

const state = {
  isLoading: false,
  data: null,
  error: null,
  noTags: false,
}

export const mutationTypes = {
  getTagsStart: '[tags] Get tags start',
  getTagsSuccess: '[tags] Get tags success',
  getTagsFailure: '[tags] Get tags failure',
}

export const actionTypes = {
  getTags: '[tags] Get tags',
}

export const getterTypes = {
  noTags: '[tags] noTags',
}

const getters = {
  [getterTypes.noTags]: (state) => {
    const tags = state.data?.length
    if (state.isLoading) return false
    if (tags === undefined) return true
    return tags === 0
  },
}

const mutations = {
  [mutationTypes.getTagsStart](state) {
    state.isLoading = true
    state.data = null
  },
  [mutationTypes.getTagsSuccess](state, payload) {
    state.isLoading = false
    state.data = payload
  },
  [mutationTypes.getTagsFailure](state) {
    state.isLoading = false
  },
}

const actions = {
  [actionTypes.getTags](context) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.getTagsStart)
      tagsApi
        .getTags()
        .then((tags) => {
          context.commit(mutationTypes.getTagsSuccess, tags)
          resolve(tags)
        })
        .catch(() => {
          context.commit(mutationTypes.getTagsFailure)
        })
    })
  },
}

export default {
  state,
  actions,
  mutations,
  getters,
}
